﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace P7UF5M03MartinezArmando
{
    public partial class Form1 : Form
    {
        public bool Saltando { get; set; }
        public bool Debug { get; set; }
        public int TiempoSalto { get; set; }
        public int Puntos { get; set; }
        public int VelocidadSalto { get; set; }
        public double VelocidadJuego { get; set; }
        public Form1()
        {
            InitializeComponent();
            GameManager.Start();
            this.Puntos = 0;
            this.Saltando = false;
            this.TiempoSalto = 20;
            this.Debug = false;
            this.VelocidadSalto = 10;
            this.VelocidadJuego = 4;
            DebugMessage.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void TeclaPulsada(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space && !Saltando)
            {
                this.Saltando = true;
            }
            if (e.KeyCode == Keys.F8)
            {
                this.Debug = !this.Debug;
                if (!this.Debug) DebugMessage.Text = "";
            }
        }

        private void GameManager_Tick(object sender, EventArgs e)
        {

            if (Debug)
            {
                DebugMessage.Text = $"Saltando: {this.Saltando}\nTop: {Dino.Top}" +
                                    $"\nTiempo Salto: {this.TiempoSalto}\nCactus 1 Left: {Cactus1.Left}";
            }

            // GRAVEDAD
            if ((this.TiempoSalto == 0) && Dino.Top < 330)
            {
                Dino.Top += this.VelocidadSalto;
            }

            // SALTO
            if (Saltando && TiempoSalto > 0)
            {
                Dino.Top -= this.VelocidadSalto;
                this.TiempoSalto--;
            }
            if (this.TiempoSalto == 0 && Dino.Top >= 338)
            {
                Saltando = false;
                this.TiempoSalto = 20;
            }
            
            // CACTUS
            foreach(Control x in this.Controls)
            {
                if (x is PictureBox && x.Tag == "Cactus")
                {
                    x.Left -= (int)this.VelocidadJuego;
                    if (x.Left < -5)
                    {
                        x.Left = (int)new Random().Next(ClientSize.Width, ClientSize.Width + 200);
                    }
                }
            }

            this.VelocidadJuego += 0.01;

            Puntuacion.Text = $"{this.Puntos} puntos";
        }
    }
}
