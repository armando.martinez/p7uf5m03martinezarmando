﻿
namespace P7UF5M03MartinezArmando
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.DebugMessage = new System.Windows.Forms.Label();
            this.Dino = new System.Windows.Forms.PictureBox();
            this.Background = new System.Windows.Forms.PictureBox();
            this.Cactus1 = new System.Windows.Forms.PictureBox();
            this.Cactus2 = new System.Windows.Forms.PictureBox();
            this.GameManager = new System.Windows.Forms.Timer(this.components);
            this.Puntuacion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Dino)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Background)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus2)).BeginInit();
            this.SuspendLayout();
            // 
            // DebugMessage
            // 
            this.DebugMessage.AutoSize = true;
            this.DebugMessage.Location = new System.Drawing.Point(12, 9);
            this.DebugMessage.Name = "DebugMessage";
            this.DebugMessage.Size = new System.Drawing.Size(35, 13);
            this.DebugMessage.TabIndex = 0;
            this.DebugMessage.Text = "label1";
            // 
            // Dino
            // 
            this.Dino.Image = ((System.Drawing.Image)(resources.GetObject("Dino.Image")));
            this.Dino.Location = new System.Drawing.Point(73, 338);
            this.Dino.Name = "Dino";
            this.Dino.Size = new System.Drawing.Size(66, 73);
            this.Dino.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dino.TabIndex = 1;
            this.Dino.TabStop = false;
            // 
            // Background
            // 
            this.Background.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Background.Location = new System.Drawing.Point(-12, 410);
            this.Background.Name = "Background";
            this.Background.Size = new System.Drawing.Size(828, 50);
            this.Background.TabIndex = 2;
            this.Background.TabStop = false;
            // 
            // Cactus1
            // 
            this.Cactus1.Image = ((System.Drawing.Image)(resources.GetObject("Cactus1.Image")));
            this.Cactus1.Location = new System.Drawing.Point(399, 337);
            this.Cactus1.Name = "Cactus1";
            this.Cactus1.Size = new System.Drawing.Size(37, 73);
            this.Cactus1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Cactus1.TabIndex = 3;
            this.Cactus1.TabStop = false;
            this.Cactus1.Tag = "Cactus";
            // 
            // Cactus2
            // 
            this.Cactus2.Image = ((System.Drawing.Image)(resources.GetObject("Cactus2.Image")));
            this.Cactus2.Location = new System.Drawing.Point(732, 350);
            this.Cactus2.Name = "Cactus2";
            this.Cactus2.Size = new System.Drawing.Size(56, 60);
            this.Cactus2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Cactus2.TabIndex = 4;
            this.Cactus2.TabStop = false;
            this.Cactus2.Tag = "Cactus";
            // 
            // GameManager
            // 
            this.GameManager.Interval = 20;
            this.GameManager.Tick += new System.EventHandler(this.GameManager_Tick);
            // 
            // Puntuacion
            // 
            this.Puntuacion.AutoSize = true;
            this.Puntuacion.Location = new System.Drawing.Point(710, 34);
            this.Puntuacion.Name = "Puntuacion";
            this.Puntuacion.Size = new System.Drawing.Size(35, 13);
            this.Puntuacion.TabIndex = 5;
            this.Puntuacion.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Puntuacion);
            this.Controls.Add(this.Cactus2);
            this.Controls.Add(this.Cactus1);
            this.Controls.Add(this.Background);
            this.Controls.Add(this.Dino);
            this.Controls.Add(this.DebugMessage);
            this.Name = "Form1";
            this.Text = "Dinosaur Game";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TeclaPulsada);
            ((System.ComponentModel.ISupportInitialize)(this.Dino)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Background)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Cactus2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DebugMessage;
        private System.Windows.Forms.PictureBox Dino;
        private System.Windows.Forms.PictureBox Background;
        private System.Windows.Forms.PictureBox Cactus1;
        private System.Windows.Forms.PictureBox Cactus2;
        private System.Windows.Forms.Timer GameManager;
        private System.Windows.Forms.Label Puntuacion;
    }
}

